module Network.HTTP.Types.Version
(
  HttpVersion(..)
, http09
, http10
, http11
, http20
)
where

import Prelude

-- | HTTP Version.
--
-- Note that the Show instance is intended merely for debugging.
data HttpVersion
    = HttpVersion {
        httpMajor :: Int
      , httpMinor :: Int
      }
-- TODO: Typeable?

instance eqHttpVersion :: Eq HttpVersion where
  eq (HttpVersion a) (HttpVersion b) = a.httpMajor == b.httpMajor && a.httpMinor == b.httpMinor

instance ordHttpVersion :: Ord HttpVersion where
  compare (HttpVersion a) (HttpVersion b) =
    if a.httpMajor == b.httpMajor
      then compare a.httpMinor b.httpMinor
      else compare a.httpMajor b.httpMajor

instance showHttpVersion :: Show HttpVersion where
  show (HttpVersion { httpMajor: major, httpMinor: minor }) = "HTTP/" <> show major <> "." <> show minor

-- | HTTP 0.9
http09 :: HttpVersion
http09 = HttpVersion { httpMajor: 0, httpMinor: 9 }

-- | HTTP 1.0
http10 :: HttpVersion
http10 = HttpVersion { httpMajor: 1, httpMinor: 0 }

-- | HTTP 1.1
http11 :: HttpVersion
http11 = HttpVersion { httpMajor: 1, httpMinor: 1 }

-- | HTTP 2.0
http20 :: HttpVersion
http20 = HttpVersion { httpMajor: 2, httpMinor: 0 }
