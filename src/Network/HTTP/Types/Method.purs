module Network.HTTP.Types.Method
(
  Method
, methodGet
, methodPost
, methodHead
, methodPut
, methodDelete
, methodTrace
, methodConnect
, methodOptions
, methodPatch
, StdMethod(..)
, parseMethod
, renderMethod
, renderStdMethod
)
where

import Prelude
import Data.Either (Either(..))
import Data.Maybe (Maybe(..))
import Data.Enum (class Enum)

-- | HTTP method (flat string type).
type Method = String

-- | HTTP Method constants.
methodGet     = renderStdMethod GET     :: Method
methodPost    = renderStdMethod POST    :: Method
methodHead    = renderStdMethod HEAD    :: Method
methodPut     = renderStdMethod PUT     :: Method
methodDelete  = renderStdMethod DELETE  :: Method
methodTrace   = renderStdMethod TRACE   :: Method
methodConnect = renderStdMethod CONNECT :: Method
methodOptions = renderStdMethod OPTIONS :: Method
methodPatch   = renderStdMethod PATCH   :: Method

-- | HTTP standard method (as defined by RFC 2616, and PATCH which is defined
--   by RFC 5789).
data StdMethod
    = GET
    | POST
    | HEAD
    | PUT
    | DELETE
    | TRACE
    | CONNECT
    | OPTIONS
    | PATCH
-- These are ordered by suspected frequency. More popular methods should go first.

derive instance eqStdMethod :: Eq StdMethod
derive instance ordStdMethod :: Ord StdMethod

instance showStdMethod :: Show StdMethod where
  show GET     = "GET"
  show POST    = "POST"
  show HEAD    = "HEAD"
  show PUT     = "PUT"
  show DELETE  = "DELETE"
  show TRACE   = "TRACE"
  show CONNECT = "CONNECT"
  show OPTIONS = "OPTIONS"
  show PATCH   = "PATCH"

instance enumStdMethod :: Enum StdMethod where
  succ GET     = Just POST
  succ POST    = Just HEAD
  succ HEAD    = Just PUT
  succ PUT     = Just DELETE
  succ DELETE  = Just TRACE
  succ TRACE   = Just CONNECT
  succ CONNECT = Just OPTIONS
  succ OPTIONS = Just PATCH
  succ PATCH   = Nothing
  pred GET     = Nothing
  pred POST    = Just GET
  pred HEAD    = Just POST
  pred PUT     = Just HEAD
  pred DELETE  = Just PUT
  pred TRACE   = Just DELETE
  pred CONNECT = Just TRACE
  pred OPTIONS = Just CONNECT
  pred PATCH   = Just OPTIONS

instance boundedStdMethod :: Bounded StdMethod where
  bottom = GET
  top    = PATCH

-- TODO: Read, Ix, Typeable?
--       https://github.com/aristidb/http-types/blob/0.12.1/Network/HTTP/Types/Method.hs#L54

-- | Convert a method 'String' to a 'StdMethod' if possible.
parseMethod :: String -> Either String StdMethod
parseMethod "GET"     = Right GET
parseMethod "POST"    = Right POST
parseMethod "HEAD"    = Right HEAD
parseMethod "PUT"     = Right PUT
parseMethod "DELETE"  = Right DELETE
parseMethod "TRACE"   = Right TRACE
parseMethod "CONNECT" = Right CONNECT
parseMethod "OPTIONS" = Right OPTIONS
parseMethod "PATCH"   = Right PATCH
parseMethod custom    = Left custom

-- | Convert an algebraic method to a 'String'.
renderMethod :: Either String StdMethod -> Method
renderMethod (Left custom) = custom
renderMethod (Right stdMethod) = show stdMethod

-- | Convert a 'StdMethod' to a 'String'.
renderStdMethod :: StdMethod -> Method
renderStdMethod = show
